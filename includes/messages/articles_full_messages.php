<?php
return [
	"nocomment" => 
	[
		"text" => "Sorry, couldn't find that linked comment! It may have been removed or you have followed a wrong link!",
		"error" => 1
	],
	"tipsent" =>
	[
		"text" => "Thank you for the correction!"
	]
];
